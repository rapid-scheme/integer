;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid integer-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid integer))
  (begin
    (define (run-tests)
      (test-begin "Integers")

      (test-equal "integer->bytevector/LSB"
	#u8(127 255)
	(integer->bytevector -129 2 #f))

      (test-equal "integer->bytevector/MSB"
	#u8(255 127)
	(integer->bytevector -129 2 #t))

      (test-equal "bytevector->integer/LSB"
	(- 65536 129)
	(bytevector->integer #u8 (127 255) #f))

      (test-equal "bytevector->integer/MSB"
	(- 65536 129)
	(bytevector->integer #u8 (255 127) #t))

      (test-end))))
