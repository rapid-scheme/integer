;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (integer->bytevector value bytes msb?)
  (let*
      ((value
        (if (negative? value)
            (+ (expt 256 bytes) value)
            value))
       (bytevector (make-bytevector bytes)))
    (do ((i 0 (+ i 1)) (value value (quotient value 256)))
        ((= i bytes) bytevector)
      (bytevector-u8-set! bytevector
			  (if msb? (- bytes i 1) i) (modulo value 256)))))

(define (bytevector->integer bytevector msb?)
  (let ((bytes (bytevector-length bytevector)))
    (let loop ((i 0) (value 0))
      (if (= i bytes)
	  value
	  (loop (+ i 1)
		(+ (* value 256)
		   (bytevector-u8-ref bytevector (if msb? i (- bytes i 1)))))))))
